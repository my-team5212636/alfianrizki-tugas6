import {combineReducers} from 'redux';
import storeReducer from './reducers/storeReducer';
import cartReducer from './reducers/cartReducer';
import transactionReducer from './reducers/transactionreducer';
import {useReducer} from 'react';
import userReducer from './reducers/userReducer';
import authReducer from './reducers/authReducer';

const rootReducer = combineReducers({
  store: storeReducer,
  cart: cartReducer,
  transaction: transactionReducer,
  user: userReducer,
  auth: authReducer,
});

export default rootReducer;
