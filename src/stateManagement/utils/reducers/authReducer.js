const initialState = {
  loggedUser: [],
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_LOGGED_USER':
      return {
        ...state,
        loggedUser: action.data,
      };
    case 'DELETE_LOG_USER':
      var newData = [...state.loggedUser];
      // newData.splice(newData, 1);
      newData.splice(0, newData.length);
      return {
        ...state,
        loggedUser: newData,
      };
    default:
      return state;
  }
};

export default authReducer;
