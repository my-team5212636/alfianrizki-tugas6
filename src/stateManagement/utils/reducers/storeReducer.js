const initialState = {
  store: [],
};

const storeReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_DATA_STORE':
      return {
        ...state,
        store: action.data,
      };

    case 'UPDATE_DATA_STORE':
      var newData = [...state.store];
      var findIndex = state.store.findIndex(value => {
        return value.id === action.data.id;
      });
      newData[findIndex] = action.data;
      return {
        ...state,
        store: newData,
      };

    case 'DELETE_DATA_STORE':
      var newData = [...state.store];
      var findIndex = state.store.findIndex(value => {
        return value.id === action.id;
      });
      newData.splice(findIndex, 1);
      return {
        ...state,
        store: newData,
      };
    default:
      return state;
  }
};

export default storeReducer;
