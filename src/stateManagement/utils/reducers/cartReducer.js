const initialState = {
  cart: [],
};

const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_DATA_CART':
      return {
        ...state,
        cart: action.data,
      };
    case 'DELETE_DATA_CART':
      var newData = [...state.cart];
      var findIndex = state.cart.findIndex(value => {
        return value.idCart === action.idCart;
      });
      newData.splice(findIndex, 1);
      return {
        ...state,
        cart: newData,
      };
    default:
      return state;
  }
};

export default cartReducer;
