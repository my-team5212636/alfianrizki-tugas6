const initialState = {
  transaction: [],
};

const transactionReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_DATA_TRANSACTION':
      return {
        ...state,
        transaction: action.data,
      };

    case 'DELETE_DATA_TRANSACTION':
      var newData = [...state.transaction];
      var findIndex = state.transaction.findIndex(value => {
        return value.idTransaction === action.idTransaction;
      });
      newData.splice(findIndex, 1);
      return {
        ...state,
        transaction: newData,
      };
    default:
      return state;
  }
};

export default transactionReducer;
