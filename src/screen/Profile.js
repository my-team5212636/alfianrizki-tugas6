import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import React from 'react';
import {useSelector, useDispatch} from 'react-redux';

export default function Profile({navigation}) {
  const {loggedUser} = useSelector(state => state.auth);
  console.log('loggedUser: ', loggedUser);

  var user = loggedUser[0];

  const dispatch = useDispatch();
  const deleteData = async ({id}) => {
    dispatch({type: 'DELETE_LOG_USER', idLog: id});
  };

  const logOut = () => {
    const id = user.idLog;
    console.log('id: ', id);
    deleteData({id});
    navigation.replace('AuthNavigation');
  };
  return (
    <View>
      <ScrollView>
        <View style={styles.topContent}>
          <Image source={require('../assets/image/img_profile.png')} />
          <Text style={styles.username}>{user.userData.nama}</Text>
          <Text style={styles.userEmail}>{user.userData.email}</Text>
          <TouchableOpacity
            style={styles.btnEdit}
            onPress={() =>
              navigation.navigate('ProfileNavigation', {screen: 'EditProfile'})
            }>
            <Text style={{color: '#050152', fontWeight: 'bold'}}>Edit</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.bottomContent}>
          <TouchableOpacity style={styles.item}>
            <Text style={{color: 'black', fontSize: 20, fontWeight: '400'}}>
              About
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.item}>
            <Text style={{color: 'black', fontSize: 20, fontWeight: '400'}}>
              Terms & Condition
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.item}
            onPress={() =>
              navigation.navigate('ProfileNavigation', {screen: 'FAQ'})
            }>
            <Text style={{color: 'black', fontSize: 20, fontWeight: '400'}}>
              FAQ
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.item}>
            <Text style={{color: 'black', fontSize: 20, fontWeight: '400'}}>
              History
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.item}>
            <Text style={{color: 'black', fontSize: 20, fontWeight: '400'}}>
              Setting
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.btnLogout} onPress={() => logOut()}>
            <View style={{flexDirection: 'row'}}>
              <Image source={require('../assets/icon/ic_logout.png')} />
              <Text style={{color: '#EA3D3D', fontSize: 15, fontWeight: '400'}}>
                Log Out
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  topContent: {
    backgroundColor: 'white',
    alignItems: 'center',
    paddingVertical: 50,
  },
  username: {
    fontSize: 25,
    color: '#050152',
    fontWeight: '800',
    marginTop: 10,
  },
  userEmail: {
    color: '#A8A8A8',
    fontSize: 10,
  },
  btnEdit: {
    backgroundColor: '#F6F8FF',
    borderRadius: 20,
    alignItems: 'center',
    paddingVertical: 5,
    paddingHorizontal: 20,
    marginTop: 20,
  },
  bottomContent: {
    backgroundColor: '#F6F8FF',
    paddingHorizontal: 17,
    paddingTop: 12,
  },
  item: {
    backgroundColor: 'white',
    paddingHorizontal: 80,
    paddingVertical: 18,
  },
  btnLogout: {
    backgroundColor: 'white',
    alignItems: 'center',
    paddingVertical: 10,
    marginTop: 20,
    marginBottom: 66,
  },
});
