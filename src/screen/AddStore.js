import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  ToastAndroid,
  ScrollView,
} from 'react-native';
import React from 'react';
import {useState, useEffect} from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {useDispatch, useSelector} from 'react-redux';
import moment from 'moment';
import {launchImageLibrary} from 'react-native-image-picker';

export default function AddStore({navigation, route}) {
  const [nama, setNama] = useState('');
  const [alamat, setAlamat] = useState('');
  const [isOpen, setisOpen] = useState(false);
  const [rating, setRating] = useState(0.0);
  const [minPrice, setMinPrice] = useState('');
  const [maxPrice, setMaxPrice] = useState('');
  const [deskripsi, setDeskripsi] = useState('');
  const [openPicker, setOpenPicker] = useState(false);
  const [closePicker, setClosePicker] = useState(false);
  const [openTime, setOpen] = useState(new Date());
  const [closeTime, setClose] = useState(new Date());
  const [isFavourite, setIsFavourite] = useState(false);
  const [selectedImage, setSelectedImage] = useState(null);

  const checkInput = () => {
    if (
      nama === '' ||
      alamat === '' ||
      rating === '' ||
      minPrice === '' ||
      maxPrice === '' ||
      deskripsi === ''
    ) {
      showEmptyToast();
    } else if (rating <= 0) {
      showRatingErrorToast();
    } else {
      route.params ? updateData() : addData();
    }
  };

  const showEmptyToast = () => {
    ToastAndroid.show('Field cannot be empty!', ToastAndroid.SHORT);
  };

  const showRatingErrorToast = () => {
    ToastAndroid.show('Rating must be  1< ', ToastAndroid.SHORT);
  };

  const showOpenPicker = () => {
    setOpenPicker(true);
  };

  const hideOpenPicker = () => {
    setOpenPicker(false);
  };

  const showClosePicker = () => {
    setClosePicker(true);
  };

  const hideClosePicker = () => {
    setClosePicker(false);
  };

  const openConfirm = openTime => {
    setOpen(openTime);
    hideOpenPicker();
  };

  const closeConfirm = closeTime => {
    setClose(closeTime);
    hideClosePicker();
  };

  const getTime = status => {
    if (status === 'OPEN') {
      var tempTime = openTime.toString().split(' ');
      var time = tempTime[4].toString().split(':');
      return `${time[0]}:${time[1]}`;
    } else if (status === 'CLOSE') {
      var tempTime = closeTime.toString().split(' ');
      var time = tempTime[4].toString().split(':');
      return `${time[0]}:${time[1]}`;
    }
  };

  const {store} = useSelector(state => state.store);

  const dispatch = useDispatch();

  const checkData = () => {
    if (route.params) {
      const data = route.params.item;
      setAlamat(data.address);
      setDeskripsi(data.description);
      setIsFavourite(data.favourite);
      setisOpen(data.isOpen);
      setMinPrice(data.minimumPrice);
      setMaxPrice(data.maximumPrice);
      setRating(data.rating);
      setSelectedImage(data.storeImage);
      setNama(data.storeName);
    }
  };

  useEffect(() => {
    checkData();
  }, []);

  const addData = () => {
    var dataStore = [...store];
    const data = {
      id: moment(new Date()).format('YYYYMMDDHHmmssSSS'),
      address: alamat,
      availableTime: `${getTime('OPEN')} - ${getTime('CLOSE')}`,
      description: deskripsi,
      favourite: isFavourite,
      isOpen: isOpen,
      minimumPrice: minPrice,
      maximumPrice: maxPrice,
      rating: rating,
      storeName: nama,
      storeImage: selectedImage,
    };
    dataStore.push(data);
    dispatch({type: 'ADD_DATA_STORE', data: dataStore});
    navigation.goBack();
  };

  const updateData = () => {
    const data = {
      id: route.params.item.id,
      address: alamat,
      availableTime: `${getTime('OPEN')} - ${getTime('CLOSE')}`,
      description: deskripsi,
      favourite: isFavourite,
      isOpen: isOpen,
      minimumPrice: minPrice,
      maximumPrice: maxPrice,
      rating: rating,
      storeImage: selectedImage,
      storeName: nama,
    };
    dispatch({type: 'UPDATE_DATA_STORE', data});
    navigation.goBack();
  };

  const deleteData = async () => {
    dispatch({type: 'DELETE_DATA_STORE', id: route.params.item.id});
    navigation.goBack();
  };

  const openImagePicker = () => {
    const options = {
      mediaType: 'photo',
      includeBase64: true,
    };

    launchImageLibrary(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('Image picker error: ', response.error);
      } else {
        let imageUri = response.assets?.[0]?.base64;
        setSelectedImage(imageUri);
      }
    });
  };
  return (
    <View style={styles.screen}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <DateTimePickerModal
          isVisible={openPicker}
          mode="time"
          display="spinner"
          is24Hour
          onConfirm={openConfirm}
          onCancel={hideOpenPicker}
        />
        <DateTimePickerModal
          isVisible={closePicker}
          mode="time"
          display="spinner"
          is24Hour
          onConfirm={closeConfirm}
          onCancel={hideClosePicker}
        />
        <Text style={styles.title}>Add Store Data</Text>
        <View style={styles.content}>
          <Text style={styles.inputTitle}>Nama Toko</Text>
          <TextInput
            placeholder="Masukkan Nama Toko"
            style={styles.inputText}
            value={nama}
            onChangeText={text => setNama(text)}
          />
          <Text style={styles.inputTitle}>Alamat</Text>
          <TextInput
            placeholder="Masukkan Alamat"
            style={styles.inputText}
            value={alamat}
            onChangeText={text => setAlamat(text)}
          />
          <Text style={styles.inputTitle}>Status Buka Tutup</Text>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 5,
              width: '75%',
              justifyContent: 'space-between',
            }}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <TouchableOpacity
                style={{
                  width: 25,
                  height: 25,
                  borderWidth: 1,
                  borderColor: 'black',
                  borderRadius: 50,
                  backgroundColor: isOpen ? 'grey' : 'white',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
                onPress={() => {
                  setisOpen(true);
                }}>
                {isOpen ? (
                  <Icon name="check" size={20} style={{color: 'white'}} />
                ) : null}
              </TouchableOpacity>
              <Text style={{marginLeft: 10, fontWeight: 'bold', fontSize: 17}}>
                Buka
              </Text>
            </View>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <TouchableOpacity
                style={{
                  width: 25,
                  height: 25,
                  borderWidth: 1,
                  borderColor: 'black',
                  borderRadius: 50,
                  backgroundColor: !isOpen ? 'grey' : 'white',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
                onPress={() => {
                  setisOpen(false);
                }}>
                {!isOpen ? (
                  <Icon name="check" size={20} style={{color: 'white'}} />
                ) : null}
              </TouchableOpacity>
              <Text style={{marginLeft: 10, fontWeight: 'bold', fontSize: 17}}>
                Tutup
              </Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 5,
              marginBottom: 5,
            }}>
            <View style={{width: '45%'}}>
              <Text style={styles.inputTitle}>Jam Buka</Text>
              <View
                style={{
                  flexDirection: 'row',
                  borderWidth: 1,
                  borderColor: '#BB2427',
                  borderRadius: 5,
                  paddingHorizontal: 10,
                  alignItems: 'center',
                  justifyContent: 'space-between',
                }}>
                <TextInput placeholder="Pilih Jam" value={getTime('OPEN')} />
                <TouchableOpacity onPress={showOpenPicker}>
                  <Icon name="clock" size={25} style={{color: 'grey'}} solid />
                </TouchableOpacity>
              </View>
            </View>
            <View style={{width: '45%'}}>
              <Text style={styles.inputTitle}>Jam Tutup</Text>
              <View
                style={{
                  flexDirection: 'row',
                  borderWidth: 1,
                  borderColor: '#BB2427',
                  borderRadius: 5,
                  paddingHorizontal: 10,
                  alignItems: 'center',
                  justifyContent: 'space-between',
                }}>
                <TextInput placeholder="Pilih Jam" value={getTime('CLOSE')} />
                <TouchableOpacity onPress={showClosePicker}>
                  <Icon name="clock" size={25} style={{color: 'grey'}} solid />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <Text style={styles.inputTitle}>Jumlah Rating</Text>
          <TextInput
            placeholder="Masukkan Jumlah Rating"
            style={styles.inputText}
            value={rating}
            onChangeText={text => setRating(text)}
          />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 5,
              marginBottom: 5,
            }}>
            <View style={{width: '45%'}}>
              <Text style={styles.inputTitle}>Harga Minimal</Text>
              <View
                style={{
                  flexDirection: 'row',
                  borderWidth: 1,
                  borderColor: '#BB2427',
                  borderRadius: 5,
                  paddingHorizontal: 10,
                  alignItems: 'center',
                  justifyContent: 'space-between',
                }}>
                <TextInput
                  value={minPrice}
                  onChangeText={text => setMinPrice(text)}
                />
              </View>
            </View>
            <View style={{width: '45%'}}>
              <Text style={styles.inputTitle}>Harga Maksimal</Text>
              <View
                style={{
                  flexDirection: 'row',
                  borderWidth: 1,
                  borderColor: '#BB2427',
                  borderRadius: 5,
                  paddingHorizontal: 10,
                  alignItems: 'center',
                  justifyContent: 'space-between',
                }}>
                <TextInput
                  value={maxPrice}
                  onChangeText={text => setMaxPrice(text)}
                />
              </View>
            </View>
          </View>
          <Text style={styles.inputTitle}>Deskripsi</Text>
          <TextInput
            placeholder="Masukkan Deskripsi"
            style={styles.inputText}
            value={deskripsi}
            onChangeText={text => setDeskripsi(text)}
          />
          <Text style={styles.inputTitle}>Gambar Toko</Text>
          <View
            style={{
              flexDirection: 'row',
              borderWidth: 1,
              borderColor: '#BB2427',
              borderRadius: 5,
              alignItems: 'center',
              paddingHorizontal: 10,
            }}>
            <TouchableOpacity onPress={openImagePicker}>
              <Icon name="image" style={{color: 'grey'}} solid size={25} />
            </TouchableOpacity>
            <TextInput
              placeholder="Pilih Gambar"
              style={{
                marginLeft: 10,
                size: 17,
                fontWeight: 'bold',
                width: '90%',
              }}
              value={selectedImage}
              onChangeText={text => setSelectedImage(text)}
            />
          </View>
          <TouchableOpacity
            style={styles.btnSimpan}
            onPress={() => checkInput()}>
            <Text style={styles.txtButton}>
              {route.params ? 'Update' : 'Simpan'}
            </Text>
          </TouchableOpacity>
          {route.params && (
            <TouchableOpacity
              style={[styles.btnSimpan, {marginTop: 10}]}
              onPress={() => deleteData()}>
              <Text style={styles.txtButton}>Hapus</Text>
            </TouchableOpacity>
          )}
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    backgroundColor: 'white',
    padding: 20,
  },
  title: {
    fontSize: 20,
    color: 'black',
    fontWeight: 'bold',
  },
  content: {
    paddingHorizontal: 10,
    paddingVertical: 20,
    marginTop: 10,
  },
  inputTitle: {
    color: '#BB2427',
    fontWeight: 'bold',
    fontSize: 15,
  },
  inputText: {
    borderWidth: 1,
    borderColor: '#BB2427',
    borderRadius: 5,
    fontSize: 17,
    fontWeight: 'bold',
    paddingHorizontal: 10,
    marginBottom: 5,
  },
  btnSimpan: {
    borderRadius: 10,
    backgroundColor: '#BB2427',
    alignItems: 'center',
    paddingVertical: 10,
    marginTop: 40,
  },
  txtButton: {
    color: 'white',
    fontSize: 17,
    fontWeight: 'bold',
  },
});
