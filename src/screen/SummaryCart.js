import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import React, {useState} from 'react';
import ModalShowSuccess from './ModalShowSuccess';
import {useDispatch, useSelector} from 'react-redux';
import moment from 'moment';

export default function SummaryCart({navigation, route}) {
  const cart = route.params.item;
  const store = cart.storeData;
  console.log('sumStore: ', store);
  const [showModal, setShowModal] = useState(false);

  const userData = {
    nama: 'Agil Bani',
    email: 'gantengdoang@dipanggang.com',
    phoneNumber: '0813763476',
    address: 'Jl. Perumnas, Condong catur, Sleman, Yogyakarta',
  };

  const {transaction} = useSelector(state => state.transaction);

  const dispatch = useDispatch();

  const addData = () => {
    const id = cart.id;
    const date = new Date();
    var dataTransaction = [...transaction];
    const data = {
      idTransaction: moment(new Date()).format('YYYYMMDDHHmmssSSS'),
      reservationCode: moment(new Date()).format('DDHHmmss'),
      reservationDate: moment(new Date()).format('DD MMMM YYYY h:mm'),
      customerData: userData,
      storeData: store,
      cartData: cart,
    };
    dataTransaction.push(data);
    dispatch({type: 'ADD_DATA_TRANSACTION', data: dataTransaction});
    deleteData({id});
    setShowModal(true);
  };

  const deleteData = async ({id}) => {
    dispatch({type: 'DELETE_DATA_CART', idCart: id});
  };
  return (
    <View style={styles.screen}>
      <ModalShowSuccess
        show={showModal}
        onClose={() => navigation.navigate('Home')}
        onClick={() =>
          navigation.navigate('TransactionNavigation', {
            screen: 'DetailTransaction',
          })
        }
      />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.segment}>
          <Text style={styles.segmentTitle}>Data Customer</Text>
          <View style={{flexDirection: 'row'}}>
            <Text style={styles.text}>{userData.nama}</Text>
            <Text
              style={{
                fontSize: 18,
                marginBottom: 10,
                color: 'black',
                marginLeft: 10,
                fontWeight: '300',
              }}>
              ({userData.phoneNumber})
            </Text>
          </View>
          <Text style={styles.text}>{userData.address}</Text>
          <Text style={styles.text}>{userData.email}</Text>
        </View>
        <View style={styles.segment}>
          <Text style={styles.segmentTitle}>Alamat Outlet Tujuan</Text>
          <View style={{flexDirection: 'row'}}>
            <Text style={styles.text}>{store.storeName}</Text>
            <Text
              style={{
                fontSize: 18,
                marginBottom: 10,
                color: 'black',
                marginLeft: 10,
                fontWeight: '300',
              }}>
              (027-343457)
            </Text>
          </View>
          <Text style={styles.text}>{store.address}</Text>
        </View>
        <View style={styles.segmentBarang}>
          <Image source={require('../assets/image/img_shoes_cart.png')} />
          <View style={{marginLeft: 13}}>
            <View style={{flexDirection: 'row', marginBottom: 10}}>
              <Text style={{color: 'black', fontWeight: '400'}}>
                {cart.merk} -
              </Text>
              <Text style={{color: 'black', fontWeight: '400'}}>
                {' '}
                {cart.color} -
              </Text>
              <Text style={{color: 'black', fontWeight: '400'}}>
                {' '}
                {cart.size}
              </Text>
            </View>
            <FlatList
              data={cart.service}
              horizontal={true}
              renderItem={({item}) => (
                <Text style={{color: '#737373', marginBottom: 10}}>
                  {item.isChecked ? `${item.service} | ` : null}
                </Text>
              )}
            />
            <Text style={{color: '#737373'}}>Note : {cart.note}</Text>
          </View>
        </View>
      </ScrollView>
      <TouchableOpacity style={styles.button} onPress={() => addData()}>
        <Text style={styles.txtButton}>Reservasi Sekarang</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: '#F6F8FF',
    paddingTop: 5,
    paddingBottom: 50,
  },
  segment: {
    backgroundColor: 'white',
    paddingHorizontal: 25,
    paddingVertical: 20,
    marginBottom: 12,
    width: '100%',
  },
  segmentTitle: {
    color: '#979797',
    fontSize: 18,
    marginBottom: 10,
  },
  text: {
    fontSize: 18,
    marginBottom: 10,
    color: 'black',
    fontWeight: '300',
  },
  segmentBarang: {
    backgroundColor: 'white',
    paddingHorizontal: 25,
    paddingVertical: 20,
    marginBottom: 50,
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
  },
  button: {
    backgroundColor: '#BB2427',
    borderRadius: 10,
    paddingVertical: 15,
    alignItems: 'center',
    position: 'absolute',
    bottom: 1,
    width: '100%',
    marginBottom: 20,
    width: '90%',
    alignSelf: 'center',
  },
  txtButton: {
    color: 'white',
    fontSize: 15,
    fontWeight: '500',
  },
});
