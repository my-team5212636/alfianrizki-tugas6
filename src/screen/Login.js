import React, {useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  ToastAndroid,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import moment from 'moment';

const Login = ({navigation, route}) => {
  const {user} = useSelector(state => state.user);
  console.log('user: ', user);

  const {logged} = useSelector(state => state.auth);
  console.log('logged: ', logged);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  var foundUser = null;
  const checkInput = () => {
    if (email === '' || password === '') {
      showEmptyToast();
    } else {
      for (var i = 0; i < user.length; i++) {
        var checkUser = user[i];
        if (email === checkUser.email && password === checkUser.password) {
          foundUser = checkUser;
          break;
        }
      }
      if (foundUser !== null) {
        addLoggedUser();
        showSuccess();
        navigation.replace('Main');
        console.log('User data: ', foundUser);
      } else {
        showWrongtoast();
      }

      // user.filter(function (user) {
      //   if (email != user.email && password != user.password) {
      //     showSuccess();
      //     navigation.replace('Main');
      //     console.log('userdata: ', user);
      //   } else {
      //     showWrongtoast();
      //   }
      // });
    }
  };

  const {loggedUser} = useSelector(state => state.auth);
  const dispatch = useDispatch();

  const addLoggedUser = () => {
    var dataUser = [...loggedUser];
    const data = {
      idLog: moment(new Date()).format('YYYYMMDDHHmmssSSS'),
      userData: foundUser,
    };
    dataUser.push(data);
    dispatch({type: 'ADD_LOGGED_USER', data: dataUser});
  };

  const showEmptyToast = () => {
    ToastAndroid.show('Field cannot be empty!', ToastAndroid.SHORT);
  };

  const showWrongtoast = () => {
    ToastAndroid.show('Wrong email or Password!', ToastAndroid.SHORT);
  };

  const showSuccess = () => {
    ToastAndroid.show('Login Success!', ToastAndroid.SHORT);
  };

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <ScrollView //component yang digunakan agar tampilan kita bisa discroll
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <KeyboardAvoidingView //component yang digunakan untuk mengatur agar keyboard tidak menutupi
          behavior="padding" //tampilan form atau text input
          enabled
          keyboardVerticalOffset={-500}>
          <Image
            source={require('../assets/image/image_login.png')} //load atau panggil asset image dari local
            style={{
              width: Dimensions.get('window').width, //atur agar lebar gambar adalah selebar layar device
              height: 317,
            }}
          />
          <View
            style={{
              width: '100%',
              backgroundColor: '#fff',
              borderTopLeftRadius: 19,
              borderTopRightRadius: 19,
              paddingHorizontal: 20,
              paddingTop: 38,
              marginTop: -20,
            }}>
            <Text
              style={{
                fontSize: 24,
                fontWeight: 'bold',
                color: 'black',
                marginBottom: 25,
              }}>
              Welcome,{'\n'}Please Login First
            </Text>
            <Text style={{color: 'red', fontWeight: 'bold'}}>Email</Text>
            <TextInput //component yang digunakan untuk memasukkan data yang kita inginkan
              placeholder="Youremail@mail.com" //pada tampilan ini, kita ingin user memasukkan email
              style={{
                marginTop: 15,
                width: '100%',
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
              }}
              keyboardType="email-address" //akan muncul tombol @ pada keyboard yang nanti akan memudahkan user mengisi email
              onChangeText={text => setEmail(text)}
            />
            <Text style={{color: 'red', fontWeight: 'bold', marginTop: 15}}>
              Password
            </Text>
            <TextInput //component yang digunakan untuk memasukkan data password
              placeholder="Password ****"
              secureTextEntry={true} //props yang digunakan untuk menyembunyikan password user
              style={{
                marginTop: 15,
                width: '100%',
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
              }}
              onChangeText={text => setPassword(text)}
            />
            <View
              style={{
                width: '100%',
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 15,
                justifyContent: 'space-between',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                {
                  //component TouchableOpacity, kita gunakan sebagai tombol
                  //menggunakan component ini sebagai tombol, karena mudah untuk di atur style dan kegunaanya
                }
                <TouchableOpacity>
                  <Image
                    source={require('../assets/icon/ic_gmail.png')} //load asset dari local
                    style={{
                      width: 30,
                      height: 30,
                      resizeMode: 'contain',
                    }}
                  />
                </TouchableOpacity>
                <TouchableOpacity>
                  <Image
                    source={require('../assets/icon/ic_facebook.png')}
                    style={{
                      width: 30,
                      height: 30,
                      marginHorizontal: 15,
                      resizeMode: 'contain',
                    }}
                  />
                </TouchableOpacity>
                <TouchableOpacity>
                  <Image
                    source={require('../assets/icon/ic_twitter.png')}
                    style={{
                      width: 30,
                      height: 30,
                      resizeMode: 'contain',
                    }}
                  />
                </TouchableOpacity>
              </View>
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontSize: 12,
                    color: '#717171',
                  }}>
                  Forgot Password?
                </Text>
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              style={{
                width: '100%',
                marginTop: 75,
                backgroundColor: '#BB2427',
                borderRadius: 8,
                paddingVertical: 15,
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() => checkInput()}>
              <Text
                style={{
                  color: '#fff',
                  fontSize: 16,
                  fontWeight: 'bold',
                }}>
                Login
              </Text>
            </TouchableOpacity>
            <View
              style={{
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 20,
                flexDirection: 'row',
              }}>
              <Text
                style={{
                  fontSize: 12,
                  color: '#717171',
                }}>
                Don't Have An Account yet?
              </Text>
              <TouchableOpacity>
                <Text
                  style={{
                    fontSize: 14,
                    color: '#BB2427',
                    marginLeft: 5,
                  }}
                  onPress={() => navigation.navigate('Register')}>
                  Register
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    </View>
  );
};
export default Login;
