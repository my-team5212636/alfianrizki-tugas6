import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  ToastAndroid,
} from 'react-native';
import React from 'react';
import {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';

export default function Register({navigation}) {
  const [nama, setNama] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPass, setConfirmPass] = useState('');

  const checkInput = () => {
    if (nama === '' || email === '' || password === '' || confirmPass === '') {
      showEmptyToast();
    } else if (password != confirmPass) {
      showPassError();
    } else {
      showSuccess();
      addData();
    }
  };

  const showEmptyToast = () => {
    ToastAndroid.show('Field cannot be empty!', ToastAndroid.SHORT);
  };

  const showPassError = () => {
    ToastAndroid.show('Password did not match!', ToastAndroid.SHORT);
  };

  const showSuccess = () => {
    ToastAndroid.show('Register Success!', ToastAndroid.SHORT);
  };

  const {user} = useSelector(state => state.user);
  const dispatch = useDispatch();

  const addData = () => {
    var dataUser = [...user];
    const data = {
      nama: nama,
      email: email,
      password: password,
    };
    dataUser.push(data);
    dispatch({type: 'ADD_USER', data: dataUser});
    navigation.navigate('Login');
  };

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <ScrollView //component yang digunakan agar tampilan kita bisa discroll
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <KeyboardAvoidingView //component yang digunakan untuk mengatur agar keyboard tidak menutupi
          behavior="padding" //tampilan form atau text input
          enabled
          keyboardVerticalOffset={-500}>
          <Image
            source={require('../assets/image/image_login.png')} //load atau panggil asset image dari local
            style={{
              width: Dimensions.get('window').width, //atur agar lebar gambar adalah selebar layar device
              height: 317,
            }}
          />
          <View
            style={{
              width: '100%',
              backgroundColor: '#fff',
              borderTopLeftRadius: 19,
              borderTopRightRadius: 19,
              paddingHorizontal: 20,
              paddingTop: 38,
              marginTop: -20,
            }}>
            <Text
              style={{
                fontSize: 24,
                fontWeight: 'bold',
                color: 'black',
                marginBottom: 25,
              }}>
              Welcome,{'\n'}Please Register
            </Text>
            <Text style={{color: 'red', fontWeight: 'bold'}}>Nama</Text>
            <TextInput
              placeholder="Enter Your Name"
              style={{
                marginTop: 15,
                width: '100%',
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
              }}
              onChangeText={text => setNama(text)}
            />
            <Text style={{color: 'red', fontWeight: 'bold', marginTop: 15}}>
              Email
            </Text>
            <TextInput
              placeholder="Youremail@mail.com"
              style={{
                marginTop: 15,
                width: '100%',
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
              }}
              keyboardType="email-address"
              onChangeText={text => setEmail(text)}
            />
            <Text style={{color: 'red', fontWeight: 'bold', marginTop: 15}}>
              Password
            </Text>
            <TextInput
              placeholder="Enter Password"
              //   secureTextEntry={true}
              style={{
                marginTop: 15,
                width: '100%',
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
              }}
              onChangeText={text => setPassword(text)}
            />
            <Text style={{color: 'red', fontWeight: 'bold', marginTop: 15}}>
              Confirm Password
            </Text>
            <TextInput
              placeholder="Re-entered Password"
              //   secureTextEntry={true}
              style={{
                marginTop: 15,
                width: '100%',
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
              }}
              onChangeText={text => setConfirmPass(text)}
            />
            <View
              style={{
                width: '100%',
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 15,
                justifyContent: 'space-between',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                {
                  //component TouchableOpacity, kita gunakan sebagai tombol
                  //menggunakan component ini sebagai tombol, karena mudah untuk di atur style dan kegunaanya
                }
                <TouchableOpacity>
                  <Image
                    source={require('../assets/icon/ic_gmail.png')} //load asset dari local
                    style={{
                      width: 20,
                      height: 20,
                      resizeMode: 'contain',
                    }}
                  />
                </TouchableOpacity>
                <TouchableOpacity>
                  <Image
                    source={require('../assets/icon/ic_facebook.png')}
                    style={{
                      width: 20,
                      height: 20,
                      marginHorizontal: 15,
                      resizeMode: 'contain',
                    }}
                  />
                </TouchableOpacity>
                <TouchableOpacity>
                  <Image
                    source={require('../assets/icon/ic_twitter.png')}
                    style={{
                      width: 20,
                      height: 20,
                      resizeMode: 'contain',
                    }}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <TouchableOpacity
              onPress={() => checkInput()}
              style={{
                width: '100%',
                marginTop: 40,
                backgroundColor: '#BB2427',
                borderRadius: 8,
                paddingVertical: 15,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  color: '#fff',
                  fontSize: 16,
                  fontWeight: 'bold',
                }}>
                Register
              </Text>
            </TouchableOpacity>
            <View
              style={{
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 20,
                flexDirection: 'row',
              }}>
              <Text
                style={{
                  fontSize: 12,
                  color: '#717171',
                }}>
                Already have an account?
              </Text>
              <TouchableOpacity>
                <Text
                  style={{
                    fontSize: 14,
                    color: '#BB2427',
                    marginLeft: 5,
                  }}
                  onPress={() => navigation.navigate('Login')}>
                  Login
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({});
