import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  FlatList,
  Alert,
} from 'react-native';
import React from 'react';
import {useSelector, useDispatch} from 'react-redux';

export default function Transactions({navigation}) {
  const {transaction} = useSelector(state => state.transaction);
  console.log('transaction: ', transaction);

  const dispatch = useDispatch();

  const deleteAlert = ({item}) => {
    const id = item.idTransaction;
    Alert.alert('Alert!', 'Are you sure want to delete?', [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {text: 'OK', onPress: () => deleteData({id})},
    ]);

    console.log('id', id);
  };

  const deleteData = async ({id}) => {
    dispatch({type: 'DELETE_DATA_TRANSACTION', idTransaction: id});
  };
  return (
    <View style={styles.screen}>
      <FlatList
        data={transaction}
        keyExtractor={index => index.toString()}
        renderItem={({item}) => (
          <TouchableOpacity
            style={styles.item}
            onLongPress={() => deleteAlert({item})}>
            <Text style={{marginBottom: 10}}>{item.reservationDate}</Text>
            <View style={{flexDirection: 'row', marginBottom: 3}}>
              <Text style={{color: 'black', fontSize: 15}}>
                {item.cartData.merk} -
              </Text>
              <Text style={{color: 'black', fontSize: 15}}>
                {' '}
                {item.cartData.color} -
              </Text>
              <Text style={{color: 'black', fontSize: 15}}>
                {' '}
                {item.cartData.size}
              </Text>
            </View>
            <FlatList
              data={item.cartData.service}
              horizontal={true}
              renderItem={({item}) => (
                <Text
                  style={{
                    color: 'black',
                    fontSize: 15,
                    fontWeight: '300',
                    marginBottom: 13,
                  }}>
                  {item.isChecked ? `${item.service} | ` : null}
                </Text>
              )}
            />
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <View style={{flexDirection: 'row'}}>
                <Text style={{color: 'black'}}>Kode Reservasi : </Text>
                <Text style={{color: 'black', fontWeight: 'bold'}}>
                  {item.reservationCode}
                </Text>
              </View>
              <View
                style={{
                  backgroundColor: '#F29C1F29',
                  borderRadius: 20,
                  paddingHorizontal: 10,
                  paddingVertical: 3,
                }}>
                <Text style={{color: '#FFC107'}}>Reserved</Text>
              </View>
            </View>
          </TouchableOpacity>
        )}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: '#F6F8FF',
    padding: 15,
  },
  item: {
    backgroundColor: 'white',
    paddingHorizontal: 12,
    paddingVertical: 18,
    borderRadius: 10,
    marginBottom: 10,
  },
});
