import {
  StyleSheet,
  Text,
  View,
  TextInput,
  ScrollView,
  Image,
  TouchableOpacity,
  FlatList,
  ToastAndroid,
} from 'react-native';
import React, {useState} from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {useDispatch, useSelector} from 'react-redux';
import moment from 'moment';
import {launchImageLibrary} from 'react-native-image-picker';

export default function Form({route, navigation}) {
  const store = route.params.store;
  console.log('store: ', store);
  const [merk, setMerek] = useState('');
  const [warna, setWarna] = useState('');
  const [ukuran, setUkuran] = useState('');
  const [catatan, setCatatan] = useState('');
  const [image, setImage] = useState('');
  const [selectedImage, setSelectedImage] = useState(null);

  const checkInput = () => {
    if (merk === '' || warna === '' || ukuran === '') {
      showEmptyToast();
    } else if (ukuran <= 0) {
      showSizeErrorToast();
    } else {
      addData();
    }
  };

  const showEmptyToast = () => {
    ToastAndroid.show('Field cannot be empty!', ToastAndroid.SHORT);
  };

  const showSizeErrorToast = () => {
    ToastAndroid.show('Size must be 1< ', ToastAndroid.SHORT);
  };

  const handleItemPress = item => {
    const updatedData = checkbox.map(x =>
      x.service === item.service ? {...x, isChecked: !x.isChecked} : x,
    );
    setCheckbox(updatedData);
  };

  const [checkbox, setCheckbox] = useState([
    {
      service: 'Ganti Sol Sepatu',
      isChecked: false,
    },
    {
      service: 'Jahit Sepatu',
      isChecked: false,
    },
    {
      service: 'Repaint Sepatu',
      isChecked: false,
    },
    {
      service: 'Cuci Sepatu',
      isChecked: false,
    },
  ]);

  const {cart} = useSelector(state => state.cart);
  const dispatch = useDispatch();

  const addData = () => {
    const date = new Date();
    var dataCart = [...cart];
    const data = {
      idCart: moment(new Date()).format('YYYYMMDDHHmmssSSS'),
      merk: merk,
      color: warna,
      size: ukuran,
      note: catatan,
      service: checkbox,
      imageProduct: image,
      storeData: store,
    };
    dataCart.push(data);
    dispatch({type: 'ADD_DATA_CART', data: dataCart});
    navigation.navigate('Cart', {store});
  };

  const openImagePicker = () => {
    const options = {
      mediaType: 'photo',
      includeBase64: true,
    };

    launchImageLibrary(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('Image picker error: ', response.error);
      } else {
        let imageUri = response.assets?.[0]?.base64;
        setSelectedImage(imageUri);
      }
    });
  };

  return (
    <View style={styles.screen}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Text style={styles.text}>Merek</Text>
        <TextInput
          onChangeText={text => setMerek(text)}
          style={styles.inputText}
          placeholder="Masukkan Merk barang"
        />
        <Text style={styles.text}>Warna</Text>
        <TextInput
          onChangeText={text => setWarna(text)}
          style={styles.inputText}
          placeholder="Warna Barang, cth : Merah - Putih"
        />
        <Text style={styles.text}>Ukuran</Text>
        <TextInput
          onChangeText={text => setUkuran(text)}
          style={styles.inputText}
          placeholder="Cth : S, M, L / 39,40"
        />
        <Text style={styles.text}>Photo</Text>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity
            style={styles.inputPhoto}
            onPress={() => openImagePicker()}>
            <Image source={require('../assets/icon/ic_camera_red.png')} />
            <Text style={{color: '#BB2427', marginTop: 5}}>Add Photo</Text>
          </TouchableOpacity>
          <Image
            style={{width: 100, height: 75, marginLeft: 25}}
            source={{uri: `data:image/png;base64,${selectedImage}`}}
          />
        </View>

        <FlatList
          data={checkbox}
          //keyExtractor={index => index.toString()}
          renderItem={({item}) => (
            <View
              key={() => item.service}
              style={{flexDirection: 'row', marginTop: 10}}>
              <TouchableOpacity
                style={{
                  width: 24,
                  height: 24,
                  borderWidth: 1,
                  borderRadius: 5,
                  borderColor: 'black',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
                onPress={() => handleItemPress(item)}>
                {item.isChecked ? (
                  <Icon name="check" style={{color: '#000000'}} />
                ) : null}
              </TouchableOpacity>
              <Text style={{marginLeft: 5}}>{item.service}</Text>
            </View>
          )}
        />
        <Text
          style={{
            color: '#BB2427',
            fontSize: 15,
            fontWeight: '500',
            marginBottom: 10,
            marginTop: 20,
          }}>
          Catatan
        </Text>
        <TextInput
          onChangeText={text => setCatatan(text)}
          placeholder="Cth : ingin ganti sol baru"
          multiline={true}
          numberOfLines={4}
          textAlignVertical="top"
          style={{
            backgroundColor: '#F6F8FF',
            width: '100%',
            borderRadius: 10,
            padding: 10,
            marginBottom: 40,
          }}
        />
        <TouchableOpacity style={styles.button} onPress={() => checkInput()}>
          <Text style={styles.txtButton}>Masukkan Keranjang</Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    paddingHorizontal: 20,
    paddingVertical: 25,
    backgroundColor: 'white',
  },
  text: {
    color: '#BB2427',
    fontSize: 15,
    fontWeight: '500',
    marginBottom: 10,
  },
  inputText: {
    backgroundColor: '#F6F8FF',
    width: '100%',
    borderRadius: 10,
    paddingHorizontal: 10,
    marginBottom: 25,
  },
  inputPhoto: {
    backgroundColor: 'white',
    paddingHorizontal: 10,
    paddingVertical: 20,
    alignItems: 'center',
    borderRadius: 10,
    borderColor: '#BB2427',
    borderWidth: 1,
    width: 100,
    marginBottom: 40,
  },
  checkboxWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 15,
  },
  button: {
    backgroundColor: '#BB2427',
    borderRadius: 10,
    paddingVertical: 15,
    alignItems: 'center',
  },
  txtButton: {
    color: 'white',
    fontSize: 20,
    fontWeight: '500',
  },
});
