import {
  FlatList,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Alert,
} from 'react-native';
import React, {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';

export default function Cart({navigation, route}) {
  const {cart} = useSelector(state => state.cart);
  console.log('cart', cart);

  const dispatch = useDispatch();

  const deleteAlert = ({item}) => {
    const id = item.idCart;
    Alert.alert('Alert!', 'Are you sure want to delete?', [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {text: 'OK', onPress: () => deleteData({id})},
    ]);

    console.log('id', id);
  };

  const deleteData = async ({id}) => {
    dispatch({type: 'DELETE_DATA_CART', idCart: id});
  };

  return (
    <View style={styles.screen}>
      <ScrollView>
        <FlatList
          data={cart}
          keyExtractor={index => index.toString()}
          renderItem={({item}) => (
            <TouchableOpacity
              style={styles.content}
              onPress={() => navigation.navigate('Summary', {item})}
              onLongPress={() => deleteAlert({item})}>
              <Image source={require('../assets/image/img_shoes_cart.png')} />
              <View style={{marginLeft: 13}}>
                <View style={{flexDirection: 'row', marginBottom: 10}}>
                  <Text style={{color: 'black', fontWeight: '400'}}>
                    {item.merk} -
                  </Text>
                  <Text style={{color: 'black', fontWeight: '400'}}>
                    {' '}
                    {item.color} -
                  </Text>
                  <Text style={{color: 'black', fontWeight: '400'}}>
                    {' '}
                    {item.size}
                  </Text>
                </View>
                <FlatList
                  data={item.service}
                  horizontal={true}
                  renderItem={({item}) => (
                    <Text style={{color: '#737373', marginBottom: 10}}>
                      {item.isChecked ? `${item.service} | ` : null}
                    </Text>
                  )}
                />
                <Text style={{color: '#737373'}}>Note : {item.note}</Text>
              </View>
            </TouchableOpacity>
          )}
        />
        {route.params && (
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              alignSelf: 'center',
              alignItems: 'center',
            }}
            onPress={() => {
              const store = route.params.store;
              navigation.navigate('Form', {store});
            }}>
            <Image source={require('../assets/icon/ic_plus.png')} />
            <Text
              style={{color: '#BB2427', marginLeft: 10, fontWeight: 'bold'}}>
              Tambah Barang
            </Text>
          </TouchableOpacity>
        )}
      </ScrollView>
      <TouchableOpacity style={styles.button}>
        <Text style={styles.txtButton}>Selanjutnya</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: '#F6F8FF',
    padding: 10,
  },
  content: {
    backgroundColor: 'white',
    paddingHorizontal: 15,
    paddingVertical: 25,
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    marginBottom: 10,
  },
  button: {
    backgroundColor: '#BB2427',
    borderRadius: 10,
    paddingVertical: 15,
    alignItems: 'center',
    position: 'absolute',
    bottom: 1,
    width: '100%',
    marginBottom: 20,
    alignSelf: 'center',
  },
  txtButton: {
    color: 'white',
    fontSize: 15,
    fontWeight: '500',
  },
});
