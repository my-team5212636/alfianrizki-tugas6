import {View, Text} from 'react-native';
import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Transactions from './screen/Transactions';
import DetailTransaction from './screen/DetailTransaction';
import Checkout from './screen/Checkout';

const Stack = createNativeStackNavigator();

export default function TransactionNavigation() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Transaction" component={Transactions} />
      <Stack.Screen name="DetailTransaction" component={DetailTransaction} />
      <Stack.Screen name="Checkout" component={Checkout} />
    </Stack.Navigator>
  );
}
