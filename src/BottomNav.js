import {View, Text, TouchableOpacity, Image} from 'react-native';
import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Transactions from './screen/Transactions';
import Profile from './screen/Profile';
import HomeNavigation from './HomeNavigation';
import TransactionNavigation from './TransactionNavigation';
import Home from './screen/Home';

const Tab = createBottomTabNavigator();

function MyNavbar({state, descriptors, navigation}) {
  return (
    <View
      style={{
        backgroundColor: 'white',
        flexDirection: 'row',
        display: 'flex',
        justifyContent: 'space-between',
        shadowOpacity: 10,
        elevation: 5,
      }}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            // The `merge: true` option makes sure that the params inside the tab screen are preserved
            navigation.navigate({name: route.name, merge: true});
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        const getIcon = label => {
          switch (label) {
            case 'Home':
              return require('../src/assets/icon/ic_home.png');

            case 'Transactions':
              return require('../src/assets/icon/ic_transaction.png');

            case 'Profile':
              return require('../src/assets/icon/ic_profile.png');
          }
        };

        return (
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityState={isFocused ? {selected: true} : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={{
              flex: 1,
              alignItems: 'center',
              height: 82,
              justifyContent: 'center',
            }}>
            <Image
              source={getIcon(label)}
              style={{
                tintColor: isFocused ? '#BB2427' : '#D8D8D8',
                marginBottom: 10,
              }}
            />
            <Text style={{color: isFocused ? '#BB2427' : '#D8D8D8'}}>
              {label}
            </Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
}

export default function BottomNavbar() {
  return (
    <Tab.Navigator
      tabBar={props => <MyNavbar {...props} />}
      screenOptions={{headerShown: false}}>
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Transactions" component={Transactions} />
      <Tab.Screen name="Profile" component={Profile} />
    </Tab.Navigator>
  );
}
